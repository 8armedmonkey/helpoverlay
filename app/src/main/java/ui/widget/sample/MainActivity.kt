package ui.widget.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.thedeanda.lorem.LoremIpsum
import kotlinx.android.synthetic.main.activity_main.*
import ui.widget.DisplayHelpCommand
import ui.widget.Highlight
import ui.widget.R

class MainActivity : AppCompatActivity() {

    private lateinit var helpCommands: List<DisplayHelpCommand>
    private var helpIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initHelpCommands()

        content.viewTreeObserver.addOnGlobalLayoutListener {
            help_overlay_view.setOnClickListener {
                help_overlay_view.displayHelp(helpCommands[helpIndex])
                helpIndex = (helpIndex + 1).rem(helpCommands.size)
            }
        }
    }

    private fun initHelpCommands() {
        helpCommands = listOf(
            DisplayHelpCommand(
                highlightSpecs = Highlight.Specs(
                    highlightType = Highlight.HighlightType.Circle,
                    highlightedView = toolbar_action_search,
                    startScale = 0f,
                    endScale = 3f,
                    durationMillis = 750L
                ),
                text = LoremIpsum.getInstance().getWords(16, 32)
            ),
            DisplayHelpCommand(
                highlightSpecs = Highlight.Specs(
                    highlightType = Highlight.HighlightType.Silhouette,
                    highlightedView = image_view,
                    startScale = 0f,
                    endScale = 1f,
                    durationMillis = 750L
                ),
                text = LoremIpsum.getInstance().getWords(16, 32)
            ),
            DisplayHelpCommand(
                highlightSpecs = Highlight.Specs(
                    highlightType = Highlight.HighlightType.RoundedRect,
                    highlightedView = edit_text,
                    startScale = 0f,
                    endScale = 1.05f,
                    durationMillis = 750L
                ),
                text = LoremIpsum.getInstance().getWords(16, 32)
            ),
            DisplayHelpCommand(
                highlightSpecs = Highlight.Specs(
                    highlightType = Highlight.HighlightType.Circle,
                    highlightedView = positive_button,
                    startScale = 0f,
                    endScale = 1.5f,
                    durationMillis = 750L
                ),
                text = LoremIpsum.getInstance().getWords(16, 32)
            ),
            DisplayHelpCommand(
                highlightSpecs = Highlight.Specs(
                    highlightType = Highlight.HighlightType.Circle,
                    highlightedView = negative_button,
                    startScale = 0f,
                    endScale = 1.5f,
                    durationMillis = 750L
                ),
                text = LoremIpsum.getInstance().getWords(16, 32)
            )
        )
    }

}