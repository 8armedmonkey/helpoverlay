package ui.widget

import android.graphics.RectF
import org.junit.Assert.*
import org.junit.Test

class RectFExtensionsTest {

    private val container = RectF(0f, 0f, 10f, 10f)

    @Test
    fun whenAreaToTheLeftExistsThenItShouldReturnNonEmptyRect() {
        RectF().also {
            RectF(2.5f, 2.5f, 7.5f, 7.5f).getAreaToTheLeft(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(2.5f, it.right)
            assertEquals(10f, it.bottom)
        }

        RectF().also {
            RectF(5f, 2.5f, 10f, 7.5f).getAreaToTheLeft(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(5f, it.right)
            assertEquals(10f, it.bottom)
        }

        RectF().also {
            RectF(10f, 2.5f, 15f, 7.5f).getAreaToTheLeft(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }
    }

    @Test
    fun whenAreaToTheLeftNotExistsThenItShouldReturnEmptyRect() {
        RectF().also {
            RectF(-5f, 2.5f, 0f, 7.5f).getAreaToTheLeft(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }

        RectF().also {
            RectF(0f, 2.5f, 5f, 7.5f).getAreaToTheLeft(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }
    }

    @Test
    fun whenHasAreaToTheLeftThenItShouldReturnTrue() {
        assertTrue(RectF(2.5f, 2.5f, 7.5f, 7.5f).hasAreaToTheLeft(container))
        assertTrue(RectF(5f, 2.5f, 10f, 7.5f).hasAreaToTheLeft(container))
        assertTrue(RectF(10f, 2.5f, 15f, 7.5f).hasAreaToTheLeft(container))
    }

    @Test
    fun whenHasNoAreaToTheLeftThenItShouldReturnFalse() {
        assertFalse(RectF(-5f, 2.5f, 0f, 7.5f).hasAreaToTheLeft(container))
        assertFalse(RectF(0f, 2.5f, 5f, 7.5f).hasAreaToTheLeft(container))
    }

    @Test
    fun whenAreaAboveExistsThenItShouldReturnNonEmptyRect() {
        RectF().also {
            RectF(2.5f, 2.5f, 7.5f, 7.5f).getAreaAbove(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(2.5f, it.bottom)
        }

        RectF().also {
            RectF(2.5f, 5f, 7.5f, 10f).getAreaAbove(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(5f, it.bottom)
        }

        RectF().also {
            RectF(2.5f, 10f, 7.5f, 15f).getAreaAbove(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }
    }

    @Test
    fun whenAreaAboveNotExistsThenItShouldReturnEmptyRect() {
        RectF().also {
            RectF(2.5f, -5f, 7.5f, 0f).getAreaAbove(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }

        RectF().also {
            RectF(2.5f, 0f, 7.5f, 5f).getAreaAbove(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }
    }

    @Test
    fun whenHasAreaAboveThenItShouldReturnTrue() {
        assertTrue(RectF(2.5f, 2.5f, 7.5f, 7.5f).hasAreaAbove(container))
        assertTrue(RectF(2.5f, 5f, 7.5f, 10f).hasAreaAbove(container))
        assertTrue(RectF(2.5f, 10f, 7.5f, 15f).hasAreaAbove(container))
    }

    @Test
    fun whenHasNoAreaAboveThenItShouldReturnFalse() {
        assertFalse(RectF(2.5f, -5f, 7.5f, 0f).hasAreaAbove(container))
        assertFalse(RectF(2.5f, 0f, 7.5f, 5f).hasAreaAbove(container))
    }

    @Test
    fun whenAreaToTheRightExistsThenItShouldReturnNonEmptyRect() {
        RectF().also {
            RectF(2.5f, 2.5f, 7.5f, 7.5f).getAreaToTheRight(container, it)

            assertEquals(7.5f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }

        RectF().also {
            RectF(-5f, 2.5f, 0f, 7.5f).getAreaToTheRight(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }

        RectF().also {
            RectF(0f, 2.5f, 5f, 7.5f).getAreaToTheRight(container, it)

            assertEquals(5f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }
    }

    @Test
    fun whenAreaToTheRightNotExistsThenItShouldReturnEmptyRect() {
        RectF().also {
            RectF(5f, 2.5f, 10f, 7.5f).getAreaToTheRight(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }

        RectF().also {
            RectF(10f, 2.5f, 15f, 7.5f).getAreaToTheRight(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }
    }

    @Test
    fun whenHasAreaToTheRightThenItShouldReturnTrue() {
        assertTrue(RectF(2.5f, 2.5f, 7.5f, 7.5f).hasAreaToTheRight(container))
        assertTrue(RectF(-5f, 2.5f, 0f, 7.5f).hasAreaToTheRight(container))
        assertTrue(RectF(0f, 2.5f, 5f, 7.5f).hasAreaToTheRight(container))
    }

    @Test
    fun whenHasNoAreaToTheRightThenItShouldReturnFalse() {
        assertFalse(RectF(5f, 2.5f, 10f, 7.5f).hasAreaToTheRight(container))
        assertFalse(RectF(10f, 2.5f, 15f, 7.5f).hasAreaToTheRight(container))
    }

    @Test
    fun whenAreaBelowExistsThenItShouldReturnNonEmptyRect() {
        RectF().also {
            RectF(2.5f, 2.5f, 7.5f, 7.5f).getAreaBelow(container, it)

            assertEquals(0f, it.left)
            assertEquals(7.5f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }

        RectF().also {
            RectF(2.5f, -5f, 7.5f, 0f).getAreaBelow(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }

        RectF().also {
            RectF(2.5f, 0f, 7.5f, 5f).getAreaBelow(container, it)

            assertEquals(0f, it.left)
            assertEquals(5f, it.top)
            assertEquals(10f, it.right)
            assertEquals(10f, it.bottom)
        }
    }

    @Test
    fun whenAreaBelowNotExistsThenItShouldReturnEmptyRect() {
        RectF().also {
            RectF(2.5f, 5f, 7.5f, 10f).getAreaBelow(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }

        RectF().also {
            RectF(2.5f, 10f, 7.5f, 15f).getAreaBelow(container, it)

            assertEquals(0f, it.left)
            assertEquals(0f, it.top)
            assertEquals(0f, it.right)
            assertEquals(0f, it.bottom)
        }
    }

    @Test
    fun whenHasAreaBelowThenItShouldReturnTrue() {
        assertTrue(RectF(2.5f, 2.5f, 7.5f, 7.5f).hasAreaBelow(container))
        assertTrue(RectF(2.5f, -5f, 7.5f, 0f).hasAreaBelow(container))
        assertTrue(RectF(2.5f, 0f, 7.5f, 5f).hasAreaBelow(container))
    }

    @Test
    fun whenHasNoAreaBelowThenItShouldReturnFalse() {
        assertFalse(RectF(2.5f, 5f, 7.5f, 10f).hasAreaBelow(container))
        assertFalse(RectF(2.5f, 10f, 7.5f, 15f).hasAreaBelow(container))
    }

}