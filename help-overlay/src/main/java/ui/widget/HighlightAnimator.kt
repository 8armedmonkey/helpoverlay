package ui.widget

import android.animation.Animator
import android.animation.ValueAnimator
import android.view.animation.OvershootInterpolator

class HighlightAnimator(
    private val startScale: Float,
    private val endScale: Float,
    private val durationMillis: Long
) {

    private lateinit var animator: Animator

    fun start(updateListener: (ValueAnimator) -> Unit) {
        animator = ValueAnimator.ofFloat(startScale, endScale).apply {
            duration = durationMillis
            interpolator = OvershootInterpolator(4f)
            addUpdateListener(updateListener)
        }

        animator.start()
    }

    fun cancel() {
        if (::animator.isInitialized) {
            animator.cancel()
        }
    }

}