package ui.widget

import android.graphics.*
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.applyDimension
import android.view.View
import androidx.core.graphics.withMatrix
import androidx.core.view.drawToBitmap
import kotlin.math.max

sealed class Highlight(
    protected var scale: Float,
    protected val endScale: Float,
    protected val translation: PointF,
    private val animator: HighlightAnimator
) {

    protected val eraserPaint = Paint().apply {
        isAntiAlias = true
        xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
    }

    protected val matrix = Matrix()

    fun startAnimation(animationUpdateListener: () -> Unit) {
        animator.start { valueAnimator ->
            scale = valueAnimator.animatedValue as Float
            animationUpdateListener.invoke()
        }
    }

    fun stopAnimation() {
        animator.cancel()
    }

    abstract fun drawShape(canvas: Canvas)

    abstract fun getRectArea(outRect: RectF)

    private class Circle(
        private val radiusPixels: Float,
        scale: Float,
        endScale: Float,
        translation: PointF,
        animator: HighlightAnimator
    ) : Highlight(scale, endScale, translation, animator) {

        override fun drawShape(canvas: Canvas) {
            matrix.apply {
                reset()
                preScale(scale, scale, 0f, 0f)
                postTranslate(translation.x, translation.y)
            }

            canvas.withMatrix(matrix) {
                drawOval(
                    -radiusPixels,
                    -radiusPixels,
                    radiusPixels,
                    radiusPixels,
                    eraserPaint
                )
            }
        }

        override fun getRectArea(outRect: RectF) {
            outRect.set(
                translation.x - endScale * radiusPixels,
                translation.y - endScale * radiusPixels,
                translation.x + endScale * radiusPixels,
                translation.y + endScale * radiusPixels
            )
        }

    }

    private class Oval(
        private val radiusPixels: PointF,
        scale: Float,
        endScale: Float,
        translation: PointF,
        animator: HighlightAnimator
    ) : Highlight(scale, endScale, translation, animator) {

        override fun drawShape(canvas: Canvas) {
            matrix.apply {
                reset()
                preScale(scale, scale, 0f, 0f)
                postTranslate(translation.x, translation.y)
            }

            canvas.withMatrix(matrix) {
                drawOval(
                    -radiusPixels.x,
                    -radiusPixels.y,
                    radiusPixels.x,
                    radiusPixels.y,
                    eraserPaint
                )
            }
        }

        override fun getRectArea(outRect: RectF) {
            outRect.set(
                translation.x - endScale * radiusPixels.x,
                translation.y - endScale * radiusPixels.y,
                translation.x + endScale * radiusPixels.x,
                translation.y + endScale * radiusPixels.y
            )
        }

    }

    private class RoundedRect(
        private val halfSizes: PointF,
        private val cornerRadiusPixels: Float,
        scale: Float,
        endScale: Float,
        translation: PointF,
        animator: HighlightAnimator
    ) : Highlight(scale, endScale, translation, animator) {

        override fun drawShape(canvas: Canvas) {
            matrix.apply {
                reset()
                preScale(scale, scale, 0f, 0f)
                postTranslate(translation.x, translation.y)
            }

            canvas.withMatrix(matrix) {
                drawRoundRect(
                    -halfSizes.x,
                    -halfSizes.y,
                    halfSizes.x,
                    halfSizes.y,
                    cornerRadiusPixels,
                    cornerRadiusPixels,
                    eraserPaint
                )
            }
        }

        override fun getRectArea(outRect: RectF) {
            outRect.set(
                translation.x - endScale * halfSizes.x,
                translation.y - endScale * halfSizes.y,
                translation.x + endScale * halfSizes.x,
                translation.y + endScale * halfSizes.y
            )
        }

    }

    private class Silhouette(
        private val bitmap: Bitmap,
        scale: Float,
        endScale: Float,
        translation: PointF,
        animator: HighlightAnimator
    ) : Highlight(scale, endScale, translation, animator) {

        override fun drawShape(canvas: Canvas) {
            matrix.apply {
                reset()
                preScale(scale, scale, 0f, 0f)
                postTranslate(translation.x, translation.y)
            }

            canvas.withMatrix(matrix) {
                drawBitmap(
                    bitmap,
                    -bitmap.width / 2f,
                    -bitmap.height / 2f,
                    eraserPaint
                )
            }
        }

        override fun getRectArea(outRect: RectF) {
            outRect.set(
                translation.x - endScale * bitmap.width / 2f,
                translation.y - endScale * bitmap.height / 2f,
                translation.x + endScale * bitmap.width / 2f,
                translation.y + endScale * bitmap.height / 2f
            )
        }

    }

    enum class HighlightType {
        Circle,
        Oval,
        RoundedRect,
        Silhouette
    }

    data class Specs(
        val highlightType: HighlightType,
        val highlightedView: View,
        val startScale: Float,
        val endScale: Float,
        val durationMillis: Long
    )

    companion object {

        fun create(specs: Specs, overlayView: View): Highlight = when (specs.highlightType) {
            HighlightType.Circle -> circle(specs, overlayView)
            HighlightType.Oval -> oval(specs, overlayView)
            HighlightType.RoundedRect -> roundedRect(specs, overlayView)
            HighlightType.Silhouette -> silhouette(specs, overlayView)
        }

        private fun circle(specs: Specs, overlayView: View): Highlight = Circle(
            radiusPixels = max(specs.highlightedView.width, specs.highlightedView.height) / 2f,
            scale = specs.startScale,
            endScale = specs.endScale,
            translation = computeTranslation(specs.highlightedView, overlayView),
            animator = HighlightAnimator(
                startScale = specs.startScale,
                endScale = specs.endScale,
                durationMillis = specs.durationMillis
            )
        )

        private fun oval(specs: Specs, overlayView: View): Highlight = Oval(
            radiusPixels = PointF(
                specs.highlightedView.width / 2f,
                specs.highlightedView.height / 2f
            ),
            scale = specs.startScale,
            endScale = specs.endScale,
            translation = computeTranslation(specs.highlightedView, overlayView),
            animator = HighlightAnimator(
                startScale = specs.startScale,
                endScale = specs.endScale,
                durationMillis = specs.durationMillis
            )
        )

        private fun roundedRect(specs: Specs, overlayView: View): Highlight = RoundedRect(
            halfSizes = PointF(
                specs.highlightedView.width / 2f,
                specs.highlightedView.height / 2f
            ),
            cornerRadiusPixels = applyDimension(
                COMPLEX_UNIT_DIP,
                8f,
                overlayView.resources.displayMetrics
            ),
            scale = specs.startScale,
            endScale = specs.endScale,
            translation = computeTranslation(specs.highlightedView, overlayView),
            animator = HighlightAnimator(
                startScale = specs.startScale,
                endScale = specs.endScale,
                durationMillis = specs.durationMillis
            )
        )

        private fun silhouette(specs: Specs, overlayView: View): Highlight = Silhouette(
            bitmap = specs.highlightedView.drawToBitmap(),
            scale = specs.startScale,
            endScale = specs.endScale,
            translation = computeTranslation(specs.highlightedView, overlayView),
            animator = HighlightAnimator(
                startScale = specs.startScale,
                endScale = specs.endScale,
                durationMillis = specs.durationMillis
            )
        )

        private fun computeTranslation(highlightedView: View, overlayView: View): PointF {
            val highlightedViewLocation = IntArray(2)
            val overlayViewLocation = IntArray(2)

            highlightedView.getLocationOnScreen(highlightedViewLocation)
            overlayView.getLocationOnScreen(overlayViewLocation)

            return PointF(
                highlightedViewLocation[0] + highlightedView.width / 2f - overlayViewLocation[0],
                highlightedViewLocation[1] + highlightedView.height / 2f - overlayViewLocation[1]
            )
        }

    }

}