package ui.widget

data class DisplayHelpCommand(
    val highlightSpecs: Highlight.Specs,
    val text: String
)