package ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.text.Layout
import android.text.TextPaint
import android.util.AttributeSet
import android.util.TypedValue.*
import android.view.View

class HelpOverlayView : View {

    private lateinit var highlight: Highlight
    private lateinit var helpTextArea: HelpTextArea

    private val textPaint: TextPaint = TextPaint().apply {
        isAntiAlias = true
    }

    private var overlayColor: Int = DEFAULT_OVERLAY_COLOR
    private var textPaddingPixels: Float = applyDimension(
        COMPLEX_UNIT_DIP,
        DEFAULT_HELP_TEXT_PADDING_DP,
        resources.displayMetrics
    )

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let {
            it.drawColor(overlayColor)

            if (::highlight.isInitialized) {
                highlight.drawShape(it)
            }

            if (::helpTextArea.isInitialized) {
                helpTextArea.drawText(it)
            }
        }
    }

    override fun onDetachedFromWindow() {
        cancelPendingHighlightJob()
        super.onDetachedFromWindow()
    }

    fun displayHelp(command: DisplayHelpCommand) {
        highlight = Highlight.create(command.highlightSpecs, this)
        helpTextArea = HelpTextArea.create(
            HelpTextArea.Specs(
                command.text,
                textPaint,
                Layout.Alignment.ALIGN_CENTER,
                textPaddingPixels
            ),
            highlight,
            this
        )

        highlight.startAnimation {
            invalidate()
        }
    }

    private fun init(attrs: AttributeSet? = null) {
        setLayerType(LAYER_TYPE_SOFTWARE, null)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(attrs, R.styleable.HelpOverlayView)

            overlayColor = typedArray.getColor(
                R.styleable.HelpOverlayView_helpOverlayColor,
                DEFAULT_OVERLAY_COLOR
            )

            textPaint.apply {
                color = typedArray.getColor(
                    R.styleable.HelpOverlayView_helpTextColor,
                    DEFAULT_HELP_TEXT_COLOR
                )

                textSize = typedArray.getDimensionPixelSize(
                    R.styleable.HelpOverlayView_helpTextSize,
                    applyDimension(
                        COMPLEX_UNIT_SP,
                        DEFAULT_HELP_TEXT_SIZE_SP,
                        resources.displayMetrics
                    ).toInt()
                ).toFloat()
            }

            textPaddingPixels = typedArray.getDimensionPixelSize(
                R.styleable.HelpOverlayView_helpTextPadding,
                applyDimension(
                    COMPLEX_UNIT_DIP,
                    DEFAULT_HELP_TEXT_PADDING_DP,
                    resources.displayMetrics
                ).toInt()
            ).toFloat()

            typedArray.recycle()
        }
    }

    private fun cancelPendingHighlightJob() {
        if (::highlight.isInitialized) {
            highlight.stopAnimation()
        }
    }

    companion object {

        private val DEFAULT_OVERLAY_COLOR = Color.argb(191, 0, 0, 0)
        private val DEFAULT_HELP_TEXT_COLOR = Color.argb(255, 255, 255, 255)
        private const val DEFAULT_HELP_TEXT_SIZE_SP = 24f
        private const val DEFAULT_HELP_TEXT_PADDING_DP = 0f

    }

}