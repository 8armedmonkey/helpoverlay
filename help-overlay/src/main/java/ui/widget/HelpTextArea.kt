package ui.widget

import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.RectF
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.text.TextUtils
import android.view.View
import androidx.core.graphics.withMatrix

class HelpTextArea private constructor(
    private val staticLayout: StaticLayout,
    private val rectArea: RectF
) {

    private val matrix = Matrix()

    fun drawText(canvas: Canvas) {
        matrix.apply {
            reset()
            postTranslate(
                rectArea.centerX() - staticLayout.width / 2,
                rectArea.centerY() - staticLayout.height / 2
            )
        }

        canvas.withMatrix(matrix) {
            staticLayout.draw(canvas)
        }
    }

    data class Specs(
        val text: String,
        val textPaint: TextPaint,
        val alignment: Layout.Alignment,
        val padding: Float
    )

    companion object {

        fun create(
            specs: Specs,
            highlight: Highlight,
            overlayView: View
        ): HelpTextArea {
            val container = RectF(
                0f,
                0f,
                overlayView.width.toFloat(),
                overlayView.height.toFloat()
            )

            val highlightRect = RectF().also {
                highlight.getRectArea(it)
            }

            val maxRect = RectF()
            val outRect = RectF()

            highlightRect.getAreaToTheLeft(container, maxRect)
            highlightRect.getAreaAbove(container, outRect)

            if (maxRect.area() < outRect.area()) {
                maxRect.set(outRect)
            }

            highlightRect.getAreaToTheRight(container, outRect)

            if (maxRect.area() < outRect.area()) {
                maxRect.set(outRect)
            }

            highlightRect.getAreaBelow(container, outRect)

            if (maxRect.area() < outRect.area()) {
                maxRect.set(outRect)
            }

            return HelpTextArea(
                staticLayout = getStaticLayout(
                    specs,
                    (maxRect.width() - 2 * specs.padding).toInt()
                ),
                rectArea = maxRect
            )
        }

        private fun getStaticLayout(specs: Specs, width: Int): StaticLayout {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                StaticLayout.Builder
                    .obtain(specs.text, 0, specs.text.length, specs.textPaint, width)
                    .setAlignment(specs.alignment)
                    .setBreakStrategy(Layout.BREAK_STRATEGY_SIMPLE)
                    .setEllipsize(TextUtils.TruncateAt.END)
                    .setEllipsizedWidth(width)
                    .setIncludePad(true)
                    .setLineSpacing(0.0f, 1.0f)
                    .build()
            } else {
                StaticLayout(
                    specs.text,
                    0,
                    specs.text.length,
                    specs.textPaint,
                    width,
                    specs.alignment,
                    1.0f,
                    0.0f,
                    true,
                    TextUtils.TruncateAt.END,
                    width
                )
            }
        }

    }

}