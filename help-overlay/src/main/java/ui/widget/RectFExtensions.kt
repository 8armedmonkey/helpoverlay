package ui.widget

import android.graphics.RectF

fun RectF.area() = width() * height()

fun RectF.getAreaToTheLeft(container: RectF, outRect: RectF) {
    if (hasAreaToTheLeft(container)) {
        outRect.set(
            container.left,
            container.top,
            left,
            container.bottom
        )
    } else {
        outRect.set(0f, 0f, 0f, 0f)
    }
}

fun RectF.hasAreaToTheLeft(container: RectF): Boolean = left > container.left

fun RectF.getAreaAbove(container: RectF, outRect: RectF) {
    if (hasAreaAbove(container)) {
        outRect.set(
            container.left,
            container.top,
            container.right,
            top
        )
    } else {
        outRect.set(0f, 0f, 0f, 0f)
    }
}

fun RectF.hasAreaAbove(container: RectF): Boolean = top > container.top

fun RectF.getAreaToTheRight(container: RectF, outRect: RectF) {
    if (hasAreaToTheRight(container)) {
        outRect.set(
            right,
            container.top,
            container.right,
            container.bottom
        )
    } else {
        outRect.set(0f, 0f, 0f, 0f)
    }
}

fun RectF.hasAreaToTheRight(container: RectF): Boolean = right < container.right

fun RectF.getAreaBelow(container: RectF, outRect: RectF) {
    if (hasAreaBelow(container)) {
        outRect.set(
            container.left,
            bottom,
            container.right,
            container.bottom
        )
    } else {
        outRect.set(0f, 0f, 0f, 0f)
    }
}

fun RectF.hasAreaBelow(container: RectF): Boolean = bottom < container.bottom